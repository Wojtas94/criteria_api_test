package pl.com.mggp.criteria_api_test.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import pl.com.mggp.criteria_api_test.entity.LiquidProduct;
import pl.com.mggp.criteria_api_test.entity.LooseProduct;
import pl.com.mggp.criteria_api_test.entity.Product;
import pl.com.mggp.criteria_api_test.entity.Shop;

/**
 *
 * @author kmaslanka
 */
public class ShopRepositoryImpl implements ShopRepositoryCustom{
    
//    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("test-criteria-api");
    
//    public static EntityManager getEntityManager() {
//	    return emf.createEntityManager();
//	}
    
    @PersistenceContext
    private EntityManager em;   

    @Override
    public List<Shop> findShops(BigDecimal volume, BigDecimal weight, BigDecimal price) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Shop> cq = cb.createQuery(Shop.class);
        Root<Shop> root = cq.from(Shop.class);
        Join<Shop, Product> product = root.join("product", JoinType.LEFT);
        
       
        Predicate p1 = cb.equal(cb.treat(product, LiquidProduct.class).get("volume"), volume);            
        Predicate p2 = cb.equal(cb.treat(product, LooseProduct.class).get("weight"), weight);        
        Predicate p3 = cb.equal(product.get("price"), price);
        
        
        cq.where(cb.and(p3, cb.or(p1, p2)));
        Query q = em.createQuery(cq);

        return q.getResultList();
    }
    
    
}
