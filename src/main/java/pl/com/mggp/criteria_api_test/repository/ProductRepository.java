/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.com.mggp.criteria_api_test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.com.mggp.criteria_api_test.entity.Product;

/**
 *
 * @author wojciech.misiaszek
 */
public interface ProductRepository extends JpaRepository<Product, Long>{
    
}
