package pl.com.mggp.criteria_api_test.repository;

import java.math.BigDecimal;
import java.util.List;
import pl.com.mggp.criteria_api_test.entity.Shop;

/**
 *
 * @author kmaslanka
 */
public interface ShopRepositoryCustom {
    
    public List<Shop> findShops(BigDecimal volume, BigDecimal weight, BigDecimal price);
    
}
