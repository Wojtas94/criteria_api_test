/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.com.mggp.criteria_api_test.entity;

import java.math.BigDecimal;
import javax.persistence.Entity;

/**
 *
 * @author wojciech.misiaszek
 */
@Entity
public class LiquidProduct extends Product {
    
    
    private BigDecimal volume;

    public LiquidProduct() {
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }
    
    
}
