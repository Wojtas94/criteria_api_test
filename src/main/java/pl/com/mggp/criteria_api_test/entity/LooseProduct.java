/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.com.mggp.criteria_api_test.entity;

import java.math.BigDecimal;
import javax.persistence.Entity;

/**
 *
 * @author wojciech.misiaszek
 */
@Entity
public class LooseProduct extends Product {

    
    private BigDecimal weight;

    public LooseProduct() {
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
    
    
    
}
