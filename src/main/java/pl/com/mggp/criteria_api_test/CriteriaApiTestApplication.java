package pl.com.mggp.criteria_api_test;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import pl.com.mggp.criteria_api_test.swing.MyJFrame;

//@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@SpringBootApplication
public class CriteriaApiTestApplication {

    public static void main(String[] args) {
        ApplicationContext context = new SpringApplicationBuilder(CriteriaApiTestApplication.class)
                .headless(false).run(args);
        MyJFrame a = context.getBean(MyJFrame.class);
        a.setVisible(true);
    }

}
