/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.com.mggp.criteria_api_test;

import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import pl.com.mggp.criteria_api_test.entity.Shop;
import pl.com.mggp.criteria_api_test.repository.ShopRepositoryCustom;
import pl.com.mggp.criteria_api_test.repository.ShopRepositoryImpl;

/**
 *
 * @author wojciech.misiaszek
 */
@DataJpaTest
public class ShopTest {

    private final ShopRepositoryCustom shoprepo = new ShopRepositoryImpl();


    @Test
    public void testFindShops() {
        List<Shop> findShops = shoprepo.findShops(BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ONE);
    }

}
